<?php 

		if($_SERVER["REQUEST_METHOD"] == "POST") {

			

			include 'questions.php';
			include 'database-helper.php';
			$dbhelper = new database_helper();
			$dbhelper->init_connection("localhost", "aidelos_umfrage", "seminarfach", "aidelos_umfrage");
			
			readfile("ergebnis_top.html");

			if ($_POST["option"] == "alter") {

				echo "<h1>Auswahl nach Alter</h1>";	

				switch ($_POST["alter_darstellung"]) {
					case 'uebersicht':
						echo "<h2>Übersicht</h2>";
						$arr = $dbhelper->get_distinct_age("test16");

						foreach ($arr as $key => $value) {
							
							echo "<h2>Alter: " . $value . "</h2>";
							create_table($dbhelper->query_age("test16",  $value));

						}
						$dbhelper->close();
						break;
					
					case 'einzel':
						echo "<h2>Alter: " . $_POST["alter"] . "</h2>";
						create_table($dbhelper->query_age("test16",  $_POST["alter"]));
						$dbhelper->close();
						break;

					case 'bereich':
						echo "<h2>Von " . $_POST["alter_von"] . " bis " . $_POST["alter_bis"] . "</h2>";
						create_table($dbhelper->query_age_range("test16",  $_POST["alter_von"], $_POST["alter_bis"]));
						$dbhelper->close();
						break;	
					default:
						# code...
						break;
				}
			}

			elseif ($_POST["option"] == "gesamt") {
				
				echo "<h1>Gesamt</h1>";
				create_table($dbhelper->query_all("test16"));
			}

			elseif ($_POST["option"] == "jahrgang") {
				
				global $jahrgang;
				echo "<h1>Auswahl nach Jahrgang</h1>";	

				switch ($_POST["jg_darstellung"]) {
					case 'uebersicht':
						echo "<h2>Übersicht</h2>";
						$arr = $dbhelper->get_distinct_jahrgang("test16");

						foreach ($arr as $key => $value) {
							
							echo "<h2>Jahrgang: " . $jahrgang[$value] . "</h2>";
							create_table($dbhelper->query_jahrgang("test16",  $value));


						}
						$dbhelper->close();
						break;
					
					case 'einzel':
						echo "<h2>Jahrgang: " . $jahrgang[$_POST["jahrgang"]] . "</h2>";
						create_table($dbhelper->query_jahrgang("test16",  $_POST["jahrgang"]));
						$dbhelper->close();
						break;

					case 'bereich':
						echo "<h2>Von " . $jahrgang[$_POST["jg_von"]] . " bis " . $jahrgang[$_POST["jg_bis"]] . "</h2>";
						create_table($dbhelper->query_jahrgang_range("test16",  $_POST["jg_von"], $_POST["jg_bis"]));
						$dbhelper->close();
						break;	
					default:
						# code...
						break;
				}
			}

		}	else {


			readfile("ergebnis.html");
			
		}

		function create_table($sql_result) {

			

			$results = array();

			
			global $types;
			global $questions;
			global $display;
			global $bereiche;	

			while($row = mysql_fetch_array($sql_result)) {



				foreach ($display as $key => $value) {
					
					
					if ($row[$key] != 0) {
						if (isset($results[$key][$row[$key]])) {

							$results[$key][$row[$key]]++;
						
						}

						else {

							$results[$key][$row[$key]] = 1;

						}

						if(isset($results[$key]["count"])){

							$results[$key]["count"]++;
						}

						else {

							$results[$key]["count"] = 1;
						}

					}
				}
			}

			
			readfile("table.html");

			

			foreach ($results as $key => $value) {
				

				echo "<tr>";
				echo "<td>" . $questions[$key] . "</td>";

				$txt = "";

				if (!(in_array($key, $bereiche))) {

					global $display;
					foreach ($display[$key] as $k => $val) {
					
					$txt .= "     <td><strong>" . $val . "</strong>" . $value[$k] . " (" . round(($value[$k]*100/$value["count"]), 2) . "%)</td>";		
				}

				// echo "<td>" . $txt . "</td>";
				echo $txt;
				}

				else {

					$txt = "";
					$temp = array();

					foreach ($value as $k => $val) {
						
						if($value != "count") {



						for ($i=1; $i < sizeof($display[$key]); $i++) { 
						
						if(is_in_range($k, $display[$key][$i - 1], $display[$key][$i])) {

							$temp[$i] += $val; 
						}
					}

					}

					}

					for ($i=1; $i < (sizeof($temp) + 1); $i++) { 
						
						$txt .= "<td>     " . "<strong>" . $display[$key][$i - 1] . "-" . $display[$key][$i ] . "</strong>" . ": " . $temp[$i] . " (" . round(($temp[$i]*100/$value["count"]), 2) . "%)</td>";
					}

					$txt .= "     <td><strong>Gesamt:</strong> " . $value["count"]. "</td>";

					echo $txt;
					
				}



				
				

				echo "</tr>";
			}

			readfile("ergebnis_bottom.html");

			

		}

		function is_in_range($val, $lower, $upper){

			return ($val > $lower && $val <= $upper);
		}
 ?>
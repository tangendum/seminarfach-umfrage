<?php 
	
	
	class database_helper
	{
		
		private $con = null;

		

		function init_connection($servername, $username, $password, $database){

			global $con;
			$con = mysql_connect($servername, $username, $password);

			if (!$con)
  				{
  					die('Konnte keine Verbindung zur Datenbank herstellen: ' . mysql_error());
  				}

  			mysql_select_db($database, $con);
		}

		function insert($table, $results)
		{

			global $con;
			
			
			
			$sql = "INSERT INTO " . $table . " (time, ";
			
			

			foreach ($results as $key => $val) {

				if(gettype($val) == "array"){

					for ($i=0; $i < count($val); $i++) { 
							
							$sql .= $key . $val[$i] . ", ";
							
						}	
				} else {

					if ($val != "" && is_numeric($val)) $sql .= $key . ", ";
					
				}
			}

			
			
			
			if(substr($sql, -2, 1) == ","){

				$sql = substr($sql, 0, strlen($sql) -2);
			}

			$sql .= ") VALUES (NOW(), ";



			

			foreach ($results as $key => $value) {
				
				if(gettype($value) == "array"){

					for ($i=0; $i < count($value); $i++) { 
							
							$sql .= "2, ";
							
						}	

				} else {

					if($key == "q15") {

						if($value != "" && is_numeric($value)) $sql .= ($value/$results["q16"]) . ", ";	
					} else {

						if($value != "" && is_numeric($value)) $sql .= $value . ", ";
					}
					
					
					
				}
				
			}

			

			if(substr($sql, -2, 1) == ","){


				$sql = substr($sql, 0, strlen($sql) -2);
			}

			if(substr($sql, -1, 1) == ","){

				$sql = substr($sql, 0, strlen($sql) -1);
			}

			$sql .= ")";

			
			
			if(!mysql_query($sql)){

			 	die("Fehler: " .  mysql_error());
			 }
		}

		function create_table($name, $anmerkung){

			$sql = "CREATE TABLE " . $name . " ( ID INT AUTO_INCREMENT PRIMARY KEY, time TIMESTAMP,";

			
			global $names;
			global $types;
			global $attributes;

			foreach ($attributes as $val) {

				$sql .= " " . $val . " INT NOT NULL DEFAULT 0,";
			}

			foreach ($types as $key => $val) {
				
				switch (gettype($types[$key])) {

					case 'integer':
						$sql .= " " . $key . " INT DEFAULT 0,";
						break;
					
					case 'string':
						if($val == "number"){
						$sql .= " " . $key . " FLOAT DEFAULT 0,";
						}

						elseif (substr($val,0 ,1) == 'c' ) {
							
							$num = intval(substr($val, 1, strlen($val) - 1));
							

							for($i = 0; $i < $num; $i++){

								$sql .= " " . $key . chr($i + 97). " INT NOT NULL DEFAULT 1,";
							}

							
						}
						break;

					default:
						# code...
						break;
				}
			}

			// if(substr($sql, -2, 1) == ","){


			// 	$sql = substr($sql, 0, strlen($sql) -3);
			// }

			if(substr($sql, -1, 1) == ","){

				$sql = substr($sql, 0, strlen($sql) -1);
			}

			if($anmerkung) {
				$sql .= " " . "anmerkung TEXT";
			}
			$sql = $sql . ")";
				
			echo $sql  . "<br />";		
			
			 if(!mysql_query($sql)){

			 	die("Fehler: " .  mysql_error());
			 }
		}

		function query_all($table){

			$cols = "";

			global $display;
			foreach ($display as $key => $value) {
				
				$cols .= $key . " , ";
			}

			if(substr($cols, -2, 1) == ","){


				$cols = substr($cols, 0, strlen($cols) -2);
			}

			$sql = "SELECT " . $cols . "FROM " . $table;

			
			
			$res = mysql_query($sql);
			return $res;
		}

		function query_jahrgang($table, $jahrgang){

			$cols = "";

			global $display;
			foreach ($display as $key => $value) {
				
				$cols .= $key . " , ";
			}

			if(substr($cols, -2, 1) == ","){


				$cols = substr($cols, 0, strlen($cols) -2);
			}

			$sql = "SELECT " . $cols . "FROM " . $table. " WHERE jahrgang = " . $jahrgang;

			$res = mysql_query($sql);
			return $res;
		}

		function query_jahrgang_range($table, $lower, $upper){

			$cols = "";

			global $display;
			foreach ($display as $key => $value) {
				
				$cols .= $key . " , ";
			}

			if(substr($cols, -2, 1) == ","){


				$cols = substr($cols, 0, strlen($cols) -2);
			}

			$sql = "SELECT " . $cols . "FROM " . $table. " WHERE jahrgang BETWEEN " . $lower . " AND ". $upper;
			
			$res = mysql_query($sql);
			return $res;
		}

		function query_age($table, $age){

			$cols = "";

			global $display;
			foreach ($display as $key => $value) {
				
				$cols .= $key . " , ";
			}

			if(substr($cols, -2, 1) == ","){


				$cols = substr($cols, 0, strlen($cols) -2);
			}

			$sql = "SELECT " . $cols . "FROM " . $table. " WHERE age = " . $age;

			$res = mysql_query($sql);
			return $res;
		}

		function query_age_range($table, $lower, $upper){

			$cols = "";

			global $display;
			foreach ($display as $key => $value) {
				
				$cols .= $key . " , ";
			}

			if(substr($cols, -2, 1) == ","){


				$cols = substr($cols, 0, strlen($cols) -2);
			}

			$sql = "SELECT " . $cols . "FROM " . $table. " WHERE age BETWEEN " . $lower . " AND ". $upper;
			
			$res = mysql_query($sql);
			return $res;
		}

		function get_distinct_jahrgang($table){

			$sql = "SELECT DISTINCT jahrgang FROM " . $table . " ORDER BY jahrgang ASC";

			$res = mysql_query($sql);


			$arr = array();

			$num = 0;
			while($row = mysql_fetch_array($res)) {

				
				$arr[$num] = $row["jahrgang"];
				$num++;
			}

			return $arr;
		}

		function get_distinct_age($table){

			$sql = "SELECT DISTINCT age FROM " . $table . " ORDER BY age ASC";

			$res = mysql_query($sql);


			$arr = array();

			$num = 0;
			while($row = mysql_fetch_array($res)) {

				
				$arr[$num] = $row["age"];
				$num++;
			}

			return $arr;
		}

		function close() {

			mysql_close();
		}

	}
 ?>


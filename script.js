$(document).ready(function() {


    var tableOffset = $("#table1").offset().top;
    var $header = $("#tableheader").clone();
    var $fixedHeader = $("#header-fixed");

    QRadio = new Array("q1", "q2", "q3", "q4", "q5","q7", "q8", 
        "q9", "q10", "q11", "q12", "q13", "q21", "q22", "q23", "q25", "q26", "q28", "q29", "q30", "q34", "q35", "q36", "q37", "q38", "q40", "q41");

    QRadioOpt = new Array("q17", "q18", "q20");

    $("select").prop("selectedIndex", -1);

    initListener();

    $(window).scroll(function(event) {

        var offset = $(this).scrollTop();

        if(offset >= tableOffset && $fixedHeader.is(":hidden")) {
            $fixedHeader.show();
        } else if(offset < tableOffset) {
            $fixedHeader.hide();
        }
    });

    $("form").submit(function() {


        resetClasses();

        $(".numberonly").val(function(index, value){

            return value.replace(",", ".");
        });

        var res = validateInput();
              
        
        return res;
    });



});

function validateInput() {

    var result = true;

    if($("select[name='jahrgang']").prop("selectedIndex") != 8) {
        var ti1 = $("#t1").val() != "";
        if(!ti1) {

            $("#t1").parent("p").addClass("notvalid");
        }
        result = result && ti1;

    }
    // var ti2 = $("#t8").val() != "";
    // if(!ti2) {

    //     $("#t8").parent("p").addClass("notvalid");
    // }
    // result = result && ti2;

    var ti2 = $("select").prop("selectedIndex") != -1;

    if(!ti2) {

        $("select").parent("p").addClass("notvalid");
    }

    result = result && ti2;


    var ti3 = $("#t2").val() != "";
    if(!ti3) {

        $("#t2").parents("tr").addClass("notvalid");
    }
    result = result && ti3;

    var ti4 = $("#t3").val() != "";
    if(!ti4) {

        $("#t3").parents("tr").addClass("notvalid");
    }
    result = result && ti4;

    var ti5 = $("#t4").val() != "";
    if(!ti5) {

        $("#t4").parents("tr").addClass("notvalid");
    }
    result = result && ti5;

    var ti7 = $("#t6").val() != "";
    if(!ti7) {

        $("#t6").parents("tr").addClass("notvalid");
    }
    result = result && ti7;

    var ti8 = $("#t7").val() != "";
    if(!ti8) {

        $("#t7").parents("tr").addClass("notvalid");
    }
    result = result && ti8;

    var ti9 = $("#t9").val() != "";
    if(!ti9) {

        $("#t9").parents("tr").addClass("notvalid");
        
    }
    result = result && ti9;

    var ti10 = $("#t10").val() != "";
    if(!ti10) {

        $("#t10").parents("tr").addClass("notvalid");
        
    }
    result = result && ti10;

    var ti11 = $("#t11").val() != "";
    if(!ti11) {

        $("#t11").parents("tr").addClass("notvalid");
        
    }
    result = result && ti11;

    var ti12 = $("#t12").val() != "";
    if(!ti12) {

        $("#t12").parents("tr").addClass("notvalid");
        
    }
    result = result && ti12;

    var ti13 = $("#t13").val() != "";
    if(!ti13) {

        $("#t13").parents("tr").addClass("notvalid");
        
    }
    result = result && ti13;

    var ti14 = $("#t14").val() != "";
    if(!ti14) {

        $("#t14").parents("tr").addClass("notvalid");
        
    }
    result = result && ti14;

    // Überprüfen, ob ein Radiobutton innerhalb der radiogroups ausgewählt ist
    for(var i = 0; i < QRadio.length; i++) {

        var item = $("input[name='" + QRadio[i] + "']:radio");
        var ri = item.is(":checked");

        if(!ri) {

            item.parents("tr").addClass("notvalid");
            result = result && ri;
            
        }
    }

    if($("#c1").is(":checked")) {

        // Radiobutton aus den "Führerscheinfragen"
        for(var i = 0; i < QRadioOpt.length; i++) {

            var itemOpt = $("input[name='" + QRadioOpt[i] + "']:radio");
            var rio = itemOpt.is(":checked");

            if(!rio) {

                itemOpt.parents("tr").addClass("notvalid");
                result = result && rio;
                
            }
        }

        var ti6 = (($("#t5").val() != "") && ($("#t5").val() != "in Liter"));
        if(!ti6) {

            $("#t5").parents("tr").addClass("notvalid");
        }
        result = result && ti6;
    }


    // Checkboxes

    // var ci1 = $("#c2").is(":checked") || $("#c3").is(":checked") || $("#c4").is(":checked");

    // if(!ci1) {

    //         $("#c2").parents("tr").addClass("notvalid");
    // }

    // result = result && ci1;

    if(!result) {

        alert("Bitte beantworte alle Fragen. Nicht beantwortete Fragen sind rot markiert.");
    }
    return result;

}

function resetClasses() {

    $("p").removeClass("notvalid");
    $("tr").removeClass("notvalid");
}

function initListener() {

   
        jQuery(".numberonly").bind("propertychange keyup input paste", function(event){

            this.value = this.value.replace(/[^0-9\.,]/g,'');


            
        });

    


    var $c1 = $("#c1");
    $c1.change(function() {

        if($("#over18").is(":hidden")) {

            $("#over18").show();

        } else {
            $("#over18").hide();
        }
    });


    $("#r93").change(function() {

        $("#anmerkungen").hide();
    });

    $("#r94").change(function() {

        $("#anmerkungen").hide();
    });

    $("#r95").change(function() {

        $("#anmerkungen").show();
    });

    // var $t1 = $("#t1");
    // $t1.blur(function() {

    //     if($t1.val() == "") {

    //         $t1.val("Jahre");
    //     }

    // });



    // $t1.focus(function() {

    //     if($t1.val() == "Jahre") {

    //         $t1.val("");
    //     }

    // });

    // var $t2 = $("#t2");
    // $t2.blur(function() {

    //     if($t2.val() == "") {

    //         $t2.val("in Euro");
    //     }

    // });

    // $t2.focus(function() {

    //     if($t2.val() == "in Euro") {

    //         $t2.val("");
    //     }

    // });

    // var $t3 = $("#t3");
    // $t3.blur(function() {

    //     if($t3.val() == "") {

    //         $t3.val("Anzahl");
    //     }

    // });

    // $t3.focus(function() {

    //     if($t3.val() == "Anzahl") {

    //         $t3.val("");
    //     }

    // });

    // var $t4 = $("#t4");
    // $t4.blur(function() {

    //     if($t4.val() == "") {

    //         $t4.val("Anzahl");
    //     }

    // });

    // $t4.focus(function() {

    //     if($t4.val() == "Anzahl") {

    //         $t4.val("");
    //     }

    // });

    

    // var $t6 = $("#t6");
    // $t6.blur(function() {

    //     if($t6.val() == "") {

    //         $t6.val("in Euro");
    //     }

    // });

    // $t6.focus(function() {

    //     if($t6.val() == "in Euro") {

    //         $t6.val("");
    //     }

    // });

    // var $t7 = $("#t7");
    // $t7.blur(function() {

    //     if($t7.val() == "") {

    //         $t7.val("Prozent");
    //     }

    // });

    // $t7.focus(function() {

    //     if($t7.val() == "Prozent") {

    //         $t7.val("");
    //     }

    // });



}


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html  xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Auswertung</title>

	<!-- <link rel="stylesheet" type="text/css" href="styles.css" /> -->
	<script src="jquery.js" type="text/javascript"></script>
	<script src="auswertung.js" type="text/javascript" ></script>
</head>
<body>

<?php  
	
	include 'errorhandling.php';
	include 'questions.php';
	include 'database-helper.php';

 	$dh = new database_helper();
 	$dh->init_connection("localhost", "aidelos_umfrage", "seminarfach", "aidelos_umfrage");
 	// $dh->create_table("test16", false);
 	 	
 	// generateRandomValues($dh);
 	$dh->insert("test16", $_POST);
 	$dh->close();

 	function generateRandomValues($database_helper) {

 		for ($i=0; $i < 300; $i++) { 
 			
 			

 			$res["age"] = rand(10,19);
 			$res["jahrgang"] = rand(0,8);

 			$res["q1"] = rand(1,5);
 			$res["q2"] = rand(1,5);
 			$res["q3"] = rand(1,5);
 			$res["q4"] = rand(1,5);
 			$res["q5"] = rand(1,5);
 			$res["q6"] = rand(1,30);
 			$res["q7"] = rand(1,5);
 			$res["q8"] = rand(1,5);
 			$res["q9"] = rand(1,5);
 			$res["q10"] = rand(1,5);
 			$res["q11"] = rand(1,5);
 			$res["q12"] = rand(1,2);
 			$res["q13"] = rand(1,2);
 			// $res["q14"] = rand(1,600) * 10;
 			$res["q15"] = rand(1,20);
 			$res["q16"] = rand(3,10);
 			$res["q17"] = rand(1,5);
 			$res["q18"] = rand(1,3);
 			$res["q19"] = rand(1,12);
 			$res["q20"] = rand(1,5);
 			$res["q21"] = rand(1,5);
 			$res["q22"] = rand(1,5);
 			$res["q23"] = rand(1,2);
 			$res["q24"] = rand(1,600) * 10;
 			$res["q25"] = rand(1,100);
 			$res["q26"] = rand(1,5);

 			switch (rand(1,7)) {
 				case 1:
 					break;
 				
 				case 2:
 					$res["q27"] = array("a");
 					break;
 				case 3:
 					$res["q27"] = array("a", "b");
 					break;
 				case 4:
 					$res["q27"] = array("a", "b", "c");
 					break;
 				case 5:
 					$res["q27"] = array("b");
 					break;
 				case 6:
 					$res["q27"] = array("b", "c");
 					break;
 				case 7:
 					$res["q27"] = array("c");
 					break;					
 				default:
 					# code...
 					break;
 			}
 			
 			$res["q28"] = rand(1,5);

 			$res["q29"] = rand(1,5);
 			$res["q30"] = rand(1,5);
 			$res["q31"] = rand(10, 9000) * 10;
 			$res["q32"] = rand(10,100 ) * 1000;
 			$res["q33"] = rand(10,200 ) * 1000;
 			$res["q34"] = rand(1,5);
 			$res["q35"] = rand(1,5);
 			$res["q36"] = rand(1,5);
 			$res["q37"] = rand(1,5);
 			$res["q38"] = rand(1,5);
 			$res["q39"] = rand(1,600) * 10;
 			$res["q40"] = rand(1,5);
 			$res["q41"] = rand(1,5);
 			
 			$database_helper->insert("test16", $res);

 		}

 	}

?>
	
	<div class="content">
		<h1>Vielen Dank!</h1>
		<h2>Die Daten wurden erfolgreich gespeichert.</h2>
		<h3 class="last">Du wirst in <span id="countdown">10</span> Sekunden auf die ursprüngliche Seite weitergeleitet.</h3>
	</div>
 </body>
</html>
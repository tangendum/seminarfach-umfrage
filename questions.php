<?php 
	
	$attributes = array("age", "jahrgang");

	$names = array("q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15",
		"q16", "q17", "q18", "q19", "q20", "q21", "q22", "q23", "q24", "q25", "q26", "q27", "q28", "q29", "q30", "q31", "q32", "q33", "q34", "q35", "q36", "q37", "q38",
		"q39", "q40", "q41");

	$questions = array(
		"q1" => "Ich achte in meinem Alltag auf einen sparsamen Umgang mit Energie.", 
		"q2" => "Ich schalte Geräte vollständig aus (kein Standby-Modus)", 
		"q3"=> "Wenn ich einen Raum verlasse schalte ich das Licht aus.", 
		"q4" => "Ich achte darauf, effektiv zu heizen (nicht mit offenem Fenster und dergleichen).",
        "q5" => "Meine Familie achtet beim Kauf von Haushaltsgeräten auf energetische Kriterien." ,
        "q6" => "Wie lange lassen sie geschätzt das Wasser „warmlaufen“, ehe sie duschen? (in sek)", 
		"q7" => "Mein Haushalt achtet beim Kochen auf das Verwenden von Deckeln und auf ein passendes Verhältnis von Herdplatte und Topf.",
		"q8" => "In meinem Haus wird gleichermaßen Tags und Nachts geheizt.", 
		"q9" => "Ich nutze effektiv Tageslicht anstelle von elektrischer Beleuchtung.", 
		"q10" => "Ich verwende schaltbare Mehrfachsteckdosen.", 
		"q11" => "Ich entsorge Energiesparlampen in den Hausmüll.", 
		"q12" => "Mein Haushalt nutzt Potovoltaik- oder Solarthermieanlagen.", 
		"q13" => "Ist Ihr Handy immer an?", 
		"q15" => "Wie viele Fernseher und PCs gibt es in ihrem Haushalt? (Summe)", 
		"q16" => "Wie viele Personen leben in ihrem Haushalt?", 
		"q17" => "Beim Autofahren achte ich auf frühes Hoch- und spätes zurückschalten",
        "q18" => "Welchen Kraftstoff verbraucht ihr Auto?", 
		"q19" => "Wie viel Kraftstoff verbraucht ihr Auto auf 100km? (Liter)", 
		"q20" => "Wenn möglich verwende ich anstelle des Autos öffentliche Verkehrsmittel.", 
        "q21" => "Ich glaube von mir, gut über Möglichkeiten des Energiesparens informiert zu sein.",
        "q22" => "Ich achte explizit aus Gründen des Umweltschutzes darauf, Energie zu sparen.", 
        "q23" => "Ich kenne die Bedeutung des untenstehenden Zeichens", 
		"q24" => "Was denken Sie, wie hoch ihre jährliche Stromrechnung war! (in €)",
        "q25" => "Schätzen sie, wie viel Prozent des jährlichen Energieumsatzes der Bundesrepublik durch private Haushalte zustande kommen!",  
        "q26" => "Meine Klasse versucht, durch auf „Kipp“ gestellte Fenster einen stetigen, aber geringen Luftaustausch zu gewährleisten.",
        "q27a" => "Ich vermute, dass die Leistung der Heizung unserer Schule zu folgenden Zeiten abgesenkt wird: Nachts",
        "q27b" => "Ich vermute, dass die Leistung der Heizung unserer Schule zu folgenden Zeiten abgesenkt wird: Am Wochenende",
        "q27c" => "Ich vermute, dass die Leistung der Heizung unserer Schule zu folgenden Zeiten abgesenkt wird: In den Ferien",
        "q28" => "Meine Klasse achtet darauf, beim Verlassen eines Unterrichtsraumes selbstständig die Fenster zu schließen und das Licht auszuschalten.",
        "q29" => "Wir versuchen im Unterricht, wenn möglich durch Nutzung des Tageslichts auf elektrische Beleuchtung zu verzichten.",
        "q30" => "Die Fachlehrer achten ihrerseits auf die Einhaltung dieser energiesparenden Empfehlungen und ermahnen gegebenenfalls die Schüler.",
        "q31" => "Schätzen sie den Stromverbrauch unserer Schule! (in kWh/a)",
		"q32" => "Schätzen sie die Heizkosten des Aue-Geest-Gymnasiums Harsefeld! (in €)",
		"q33" => "Schätzen sie die daraus resultierenden Stromkosten (in €)",
		"q34" => "Ich glaube, dass insgesamt ein sparsamer Umgang mit Energie an unserer Schule stattfindet", 
        "q35" => "Ich sehe beim Umgang mit Strom, Wasser oder der Heizung an unserer Schule Optimierungsbedarf",
        "q36" => "Ich glaube, dass im Rahmen des Projekts 50/50 noch Energie an unserer Schule ingespart werden kann.",
        "q37" => "Wir versuchen, durch weit geöffnete Fenster ein kurzes, aber intensives Stoßlüften zu vollziehen.",
        "q38" => "Wir versuchen, Energiesparlampen oder LED-Leuchten anstelle von normalen Glühlampen zu verwenden.",
        "q39" => "Was denken Sie, wie hoch ihre jährliche Wasserrechnung war! (in €)",
        "q40" => "Mein Haushalt nutzt Regenwasser für Toilettenspülung und dergleichen.",  
        "q41" => "Waschmachine und Spülmaschine werden bei uns nur vollständig gefüllt gestartet."   
		);

	$types = array(
		"q1" => 5, 
		"q2" => 5, 
		"q3"=> 5, 
		"q4" => 5, 
		"q5" => 5 , 
		"q6" => "number", 
		"q7" => 5, 
		"q8" => 5, 
		"q9" => 5, 
		"q10" => 5, 
		"q11" => 5, 
		"q12" => 2, 
		"q13" => 2, 
		"q15" => "number", 
		"q16" => "number", 
		"q17" => 5, 
		"q18" => 3, 
		"q19" => "number", 
		"q20" => 5, 
		"q21" => 5, 
		"q22" => 5, 
		"q23" => 2, 
		"q24" => "number", 
		"q25" => "number", 
		"q26" => 5, 
		"q27" => "c3", 
		"q28" => 5,
		"q29" => 5,
		"q30" => 5,
		"q31" => "number",
		"q32" => "number",
		"q33" => "number",
		"q34" => 5,
		"q35" => 5,
		"q36" => 5,
		"q37" => 5,
		"q38" => 5,
		"q39" => "number",
		"q40" => 5,
		"q41" => 5
		);

	$display = array(
		// Persönlicher und alltäglicher Bereich 
		"q1" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q2" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q3" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q4" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q5" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: ") , 
		"q7" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q8" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q9" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q10" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q38" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q11" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q12" => array( 1 => "Ja: ", 2 => " Nein: ", "count" => " Gesamt: "), 
		"q13" => array( 1 => "Ja: ", 2 => " Nein: ", "count" => " Gesamt: "), 
		"q15" => array(0, 5, 10, 15, 20), 
		"q16" => array(0, 3, 5, 7, 10), 

		// Auto 
		"q17" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q18" => array( 1 => "Benzin: ", 2 => " Diesel: ", 3 => " Gas, Sonstige: ", "count" => " Gesamt: "), 
		"q19" => array(0, 4, 8, 12), 
		"q20" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 

		// Wasser
		"q6" => array(0, 10, 20, 30),
		"q40" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q40" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q41" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),

		// Wissen
		"q21" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q22" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q23" => array( 1 => "Ja: ", 2 => " Nein: ", "count" => " Gesamt: "),  
		"q24" => array(0, 1000, 2000, 3000, 4000, 5000, 6000), 
		"q39" => array(0, 1000, 2000, 3000, 4000, 5000, 6000),
		"q25" => array(1 => " 9,7%: ", 2 => " 21,3%: ", 3 => " 28,8%: ", 4 => " 35,1%: ", 5 => " 45,4%: ",  "count" => " Gesamt: "),

		// Schule 
		"q26" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q37" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "), 
		"q27a" => array( 1 => "Ja: ", 2 => " Nein: ", "count" => " Gesamt: "),
		"q27b" => array( 1 => "Ja: ", 2 => " Nein: ", "count" => " Gesamt: "),
		"q27c" => array( 1 => "Ja: ", 2 => " Nein: ", "count" => " Gesamt: "), 
		"q28" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q29" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q30" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q31" => array(0, 25000, 50000, 75000, 100000),
		"q33" => array(0, 50000, 100000, 150000, 200000),
		"q32" => array(0, 25000, 50000, 75000, 100000),
		"q34" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q35" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: "),
		"q36" => array( 1 => "voll zu: ", 2 => " eher zu: ", 3 => " teilweise zu: ", 4 => " eher nicht zu: ", 5 => " nicht zu: ", "count" => " Gesamt: ")
		
		);

		$bereiche = array("q6", "q15", "q16", "q19", "q24", "q39", "q31", "q32", "q33");

		$jahrgang = array(
			0 => 5,
			1 => 6,
			2 => 7,
			3 => 8,
			4 => 9,
			5 => 10,
			6 => 11,
			7 => 12,
			8 => "Lehrer"

			);
 ?>
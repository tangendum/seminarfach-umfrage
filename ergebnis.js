$(document).ready(function()  {

	$("input:radio").attr("checked", false);
	$(".alter").hide();
	$(".jahrgang").hide();

	$("#r1").change(function() {

		$(".alter").hide();
		$(".jahrgang").hide();
	});

	$("#r2").change(function() {

		$(".alter").show();
		$(".jahrgang").hide();
	});

	$("#r3").change(function() {

		$(".alter").hide();
		$(".jahrgang").show();
	});


});